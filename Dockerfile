
FROM golang:1.15 as builder

WORKDIR /build

COPY . .

RUN CGO_ENABLED=0 go build

# Choose alpine as a base image to make this useful for CI, as many
# CI tools expect an interactive shell inside the container
FROM alpine:3.12 as production

COPY --from=builder /build/live-server /usr/bin/live-server
RUN chmod +x /usr/bin/live-server

ARG VERSION=none
LABEL version=1.1.0

WORKDIR /workdir

CMD live-server
