build:
	go mod tidy && go mod vendor
	go build -o ./bin/live-server && chmod +X ./bin/*
	echo "Executable Ready in ./bin/live-server"

docker:
	docker build -t metalogic/live-server:latest .

format: 
	gofmt -l -s -w .

# Compiled with Arch Go package guidelines and removed whitespace.
cross-compile:
	env GOOS=linux GOARCH=arm go build -o ./release/live-server-linux-arm32 -ldflags "-s -w" -trimpath -mod=readonly
	env GOOS=linux GOARCH=arm64 go build -o ./release/live-server-linux-arm64 -ldflags "-s -w" -trimpath -mod=readonly
	env GOOS=linux GOARCH=386 go build -o ./release/live-server-linux-x32 -ldflags "-s -w" -trimpath -mod=readonly
	env GOOS=linux GOARCH=amd64 go build -o ./release/live-server-linux-x64 -ldflags "-s -w" -trimpath -mod=readonly
	env GOOS=darwin GOARCH=amd64 go build -o ./release/live-server-mac-x64 -ldflags "-s -w" -trimpath -mod=readonly 
	env GOOS=windows GOARCH=386 go build -o ./release/live-server-windows-x32.exe -ldflags "-s -w" -trimpath -mod=readonly
	env GOOS=windows GOARCH=amd64 go build -o ./release/live-server-windows-x64.exe -ldflags "-s -w" -trimpath -mod=readonly

clean:
	rm -rf ./bin/*
	rm -rf ./release/*
	rm -rf ./vendor/*

run:
	./bin/live-server

run-docker:
	docker run -p 9000:9000 -v $(PWD):/workdir metalogic/live-server

